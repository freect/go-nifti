# NIFTI

This the FreeCT fork of https://github.com/henghuang/nifti.  FreeCT will continue to offer it under the MIT license for anyone that wants to use it and our subsequent changes.  We'd appreciate a shout out and/or citation if you use it, but not strictly required.  If you do cite this repo, please also attribute the original henghuan/nifti library.  I will update this README with more info at some point in the future.

## Original README

A simple NIFTI io lib write in pure Golang. Only provide the most basic read/write functions.
See examples for the usage.

# INSTALLATION
go get github.com/henghuang/nifti
